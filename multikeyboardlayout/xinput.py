#/usr/bin/env python
# -*- coding: utf-8 -*-


from gi.repository import GUdev
from gi.repository import GLib
import sys
import os
import re
import subprocess
import ConfigParser
from subprocess import Popen

# ⎡ Virtual core pointer                    	id=2	[master pointer  (3)]
# ⎜   ↳ Virtual core XTEST pointer              	id=4	[slave  pointer  (2)]
# ⎜   ↳ SynPS/2 Synaptics TouchPad              	id=16	[slave  pointer  (2)]
# ⎣ Virtual core keyboard                   	id=3	[master keyboard (2)]
#    ↳ Virtual core XTEST keyboard             	id=5	[slave  keyboard (3)]
#    ↳ Power Button                            	id=6	[slave  keyboard (3)]
#    ↳ Video Bus                               	id=7	[slave  keyboard (3)]
#    ↳ Video Bus                               	id=8	[slave  keyboard (3)]
#    ↳ Power Button                            	id=9	[slave  keyboard (3)]
#    ↳ Sleep Button                            	id=10	[slave  keyboard (3)]
#    ↳ Laptop_Integrated_Webcam_HD             	id=11	[slave  keyboard (3)]
#    ↳ AT Translated Set 2 keyboard            	id=15	[slave  keyboard (3)]
#    ↳ Dell WMI hotkeys                        	id=17	[slave  keyboard (3)]

match_keyboard_re = re.compile( r"""
                               (master|slave) # Either a master or slave
                               \s+              # Followed by at least 1 space
                               keyboard         # and it's a keyboard
                               """,
                               re.VERBOSE)

keyboard_name_and_id_re = \
    re.compile(r"""
               \s                           # followed by a space
               (?P<name>\w                  # at least one symbol
               .*                      # as many alpha and spaces
               )
               \s+                          # as many spaces as it takes
               id=(?P<id>[0-9]+)            # one numeric id
               \s+
               \[(?P<type>slave|master)     # slave or master
               \s+keyboard
               """ ,
               re.VERBOSE)


# @todo: Migrate to gudev to read all the hid devices
# xinput --list-props <id>
# Device 'Microsoft Microsoft® 2.4GHz Transceiver v7.0':
#   Device Enabled (135):	1
#   Coordinate Transformation Matrix (137):	1.000000, 0.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000, 0.000000, 1.000000
#   Device Product ID (254):	1118, 1861
#   Device Node (255):	"/dev/input/event7"
#
# udevadm info --query=all --path=/sys/class/input/event7
# [~] > udevadm info --query=all --path=/sys/class/input/event7
# P: /devices/pci0000:00/0000:00:1d.0/usb2/2-1/2-1.2/2-1.2:1.0/input/input19/event7
# N: input/event7
# S: input/by-id/usb-Microsoft_Microsoft®_2.4GHz_Transceiver_v7.0-event-kbd
# S: input/by-path/pci-0000:00:1d.0-usb-0:1.2:1.0-event-kbd
# E: DEVLINKS=/dev/input/by-id/usb-Microsoft_Microsoft®_2.4GHz_Transceiver_v7.0-event-kbd /dev/input/by-path/pci-0000:00:1d.0-usb-0:1.2:1.0-event-kbd
# E: DEVNAME=/dev/input/event7
# E: DEVPATH=/devices/pci0000:00/0000:00:1d.0/usb2/2-1/2-1.2/2-1.2:1.0/input/input19/event7
# E: ID_BUS=usb
# E: ID_INPUT=1
#
#E: ID_INPUT_KEY=1
# E: ID_INPUT_KEYBOARD=1
# E: ID_MODEL=Microsoft®_2.4GHz_Transceiver_v7.0
# E: ID_MODEL_ENC=Microsoft®\x202.4GHz\x20Transceiver\x20v7.0
# E: ID_MODEL_ID=0745
# E: ID_PATH=pci-0000:00:1d.0-usb-0:1.2:1.0
# E: ID_PATH_TAG=pci-0000_00_1d_0-usb-0_1_2_1_0
# E: ID_REVISION=0663
# E: ID_SERIAL=Microsoft_Microsoft®_2.4GHz_Transceiver_v7.0
# E: ID_TYPE=hid
# E: ID_USB_DRIVER=usbhid
# E: ID_USB_INTERFACES=:030101:030102:030000:
# E: ID_USB_INTERFACE_NUM=00
# E: ID_VENDOR=Microsoft
# E: ID_VENDOR_ENC=Microsoft
# E: ID_VENDOR_ID=045e
# E: MAJOR=13
# E: MINOR=71
# E: SUBSYSTEM=input
# E: USEC_INITIALIZED=5050012
# E: XKBLAYOUT=br
# E: XKBMODEL=pc105


# @todo: Filter devices that are not of type hid (e.g. video)
#
def get_available_keyboards():
    """
    Get a list of available keyboards and their ids

    @return: [tuple]
    @rtype: [tuple]
    """

    keyboards = []

    p = Popen(["xinput", "list"],
              stdout = subprocess.PIPE, stderr=subprocess.STDOUT)

    if (p is None) or (p.stdout is None):
        print >> sys.stderr, "Error executing xinput"
        return keyboards

    for line in (p.stdout.readlines()):
        m = keyboard_name_and_id_re.search(line)
        if (m):
            keyboards.append( (m.groupdict()["name"].strip(),
                               m.groupdict()["id"].strip()) )
    p.terminate()

    return keyboards

__all__ = [ 'get_available_keyboards' ]
