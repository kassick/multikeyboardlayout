#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Multiple Keyboard Layout configurationh tool

Usage:
    multiplekeyboardlayout [--session]

Options:
    --session           Do not show window

"""

from gi.repository import GObject
from gi.repository import GUdev
from gi.repository import Gtk
from gi.repository import Gio
from docopt import docopt

from multikeyboardlayout.utils import *
from multikeyboardlayout.layoutwindow import *
from multikeyboardlayout.xinput import *
from multikeyboardlayout.kbd_conf_manager import *
from multikeyboardlayout.dbus_watcher import DBusWatcher

import sys
import os.path
import xdg.BaseDirectory
from xdg.DesktopEntry import DesktopEntry



MENU_XML = """
<interface>
    <menu id='app-menu'>
        <section>
            <item>
                <attribute name='label' translatable='yes'>_About</attribute>
                <attribute name='action'>app.about</attribute>
            </item>
        </section>
        <section>
            <item>
                <attribute name='label' translatable='yes'>_Quit the Daemon</attribute>
                <attribute name='action'>app.quit</attribute>
                <attribute name='accel'>&lt;Primary&gt;q</attribute>
            </item>
        </section>
    </menu>
</interface>
"""


class MultipleLayoutApplication(Gtk.Application):
    """
    Constructs the application object
    """

    __gsignals__ = {
        "keyboard-added": (GObject.SIGNAL_RUN_FIRST, None,  () ),
    }

    def __init__(self, conf_manager, do_show=True):
        """

        :type conf_manager: kbd_conf_manager.KeyboardConfManager
        """

        Gtk.Application.__init__(self,
                                 register_session=True,
                                 flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
                                 application_id = \
                                 "org.kazic.MultipleKeyboardLayoutApplication")
        self.do_show = do_show
        self.conf_manager = conf_manager
        self.win = None
        #self.connect("startup", self.startup)
        self.connect("keyboard-added", self.on_keyboard_added)

        # Connect gudev signals
        self.udev_client = GUdev.Client(subsystems=["hid"])
        self.udev_client.connect("uevent", self.on_keyboard_added)

        # Connect dbus signals
        self.dbus_watcher = DBusWatcher(self)


    def do_activate(self):
        active_windows = self.get_windows()
        if len(active_windows) > 0:
            active_windows[0].show_all()
            active_windows[0].present()
        else:
            self.win = LayoutWindow(self)
            if self.do_show:
                self.win.show_all()

    def do_command_line(self, args):
        Gtk.Application.do_command_line(self, args)
        arguments = docopt(__doc__, argv=args.get_arguments()[1:])
        self.do_show = not(arguments["--session"])
        self.do_activate()
        return 0


    def do_startup(self):
        Gtk.Application.do_startup(self)

        action = Gio.SimpleAction(name="about")
        action.connect("activate", self.about_activated)
        self.add_action(action)

        action = Gio.SimpleAction(name="quit")
        action.connect("activate", lambda a,b: self.quit() )
        self.add_action(action)

        builder = Gtk.Builder()
        builder.add_from_string(MENU_XML)

        self.set_app_menu(builder.get_object("app-menu"))

    def about_activated(self, widget, data):
        about_dialog = \
            Gtk.AboutDialog(program_name = 'Multiple Keyboard Layout',
                            title = 'About Multiple Keyboard Layout',
                            comments = 'Maps one layout for each configured keyboard')
                                    #'logo', todo,

        about_dialog.run()
        about_dialog.destroy()
        return True

    def on_keyboard_added(self, *args, **kwargs):
        """
        Happens when a new usb device is detected OR computer came back from sleep
        """
        windows = self.get_windows()
        if (len(windows) > 0):
            # should always be
            windows[0].emit("reload_keyboards")
        map_keyboards_from_conf(self.conf_manager)


    def get_autostart_desktop_file(self):
        return os.path.join(xdg.BaseDirectory.xdg_config_home,
                            "autostart",
                            "MultipleKeyboardLayout.desktop")

    def get_application_desktop_file(self):
        #  /usr/local/bin/program -> /usr/local
        prefix =  \
            os.path.normpath(
                os.path.join( os.path.dirname(sys.argv[0]),
                              '../'))

        return os.path.join(prefix, "share",
                                    "applications",
                                    "MultipleKeyboardLayout.desktop")


    def check_is_in_autostart(self):

        df = self.get_autostart_desktop_file()

        if os.path.exists(df):
            try:
                de = DesktopEntry(df)

                # Check if there is an override
                # No idea if this works for KDE...
                #
                key = "Autostart-enabled"
                cur_desktop = os.getenv("XDG_CURRENT_DESKTOP")

                if cur_desktop:
                    key = "X-%s-%s" % (cur_desktop, key)

                autostart_enabled = de.get(key, type=bool)

                if len(autostart_enabled) == 0:
                    # undefined, so the existance of the file is enough
                    return True
                else:
                    return autostart_enabled == 'true'
            except:
                return False
            pass

        else:
            # autostart file does not exist
            return False

    def enable_session_autostart(self):

        df = self.get_application_desktop_file()
        adf = self.get_autostart_desktop_file()

        if not(os.path.exists(df)):
            dialog = Gtk.MessageDialog(self.win,
                                       Gtk.DialogFlags.MODAL,
                                       Gtk.MessageType.ERROR,
                                       Gtk.ButtonsType.CLOSE,
                                       "Desktop file does not exist in your installation!")
            dialog.run()
            dialog.destroy()
            return False
        else:
            autostart_dir = os.path.dirname(adf)
            if not(os.path.exists(autostart_dir)):
                os.makedirs(autostart_dir)

            # Enable session mode
            try:
                de = DesktopEntry(df)
                de.set('Exec', de.get('Exec') + ' --session')
                de.write(adf)

                return True
            except:
                dialog = Gtk.MessageDialog(self.win,
                                           Gtk.DialogFlags.MODAL,
                                           Gtk.MessageType.ERROR,
                                           Gtk.ButtonsType.CLOSE,
                                           "Could not set autostart")
                dialog.run()
                dialog.destroy()
                return False
            pass
        return False

    def disable_session_autostart(self):
        adf = self.get_autostart_desktop_file()

        if not(os.path.exists(adf)):
            # Does not exist, why bother
            return True
        else:
            # Disable autostart
            key = "Autostart-enabled"
            cur_desktop = os.getenv("XDG_CURRENT_DESKTOP")

            if cur_desktop:
                key = "X-%s-%s" % (cur_desktop, key)

            try:
                # Disable, but leave it available for session management
                de = DesktopEntry(adf)
                de.set(key, "false")
                de.write(adf)

                return True
            except:
                dialog = Gtk.MessageDialog(self.win, Gtk.DialogFlags.MODAL,
                                            Gtk.ButtonsType.CLOSE,
                                            "Could not reset autostart")
                dialog.run()
                dialog.destroy()
                return False
            pass
        return True



