#!/usr/bin/env python
# -*- coding: utf-8 -*-

# File: "/home/kassick/Source/MultiKeyboardLayout/multikeyboardlayout/dbus_watcher.py"
# Created: "Sex, 04 Abr 2014 17:11:26 +0200 (kassick)"
# Updated: "Sex, 04 Abr 2014 17:22:55 +0200 (kassick)"
# $Id$
# Copyright (C) 2014, Rodrigo Virote Kassick <rvkassick@inf.ufrgs.br>

from gi.repository import Gio

class DBusWatcher:
    def __init__(self, app):
        self.app = app
        self.sleep_subscriber_id = 0

        # Connect to the system bus and connect to the login manager sleep
        # signal
        #
        self.dbus_client = Gio.bus_get_sync(Gio.BusType.SYSTEM, None)
        Gio.bus_watch_name_on_connection(self.dbus_client,
                                         "org.freedesktop.login1",
                                         Gio.BusNameWatcherFlags.AUTO_START,
                                         self.login_manager_name_appeared,
                                         self.login_manager_name_desappeared)


    def on_sleep_signal(self,
                        connection,
                        sender_name,
                        object_path,
                        interface_name,
                        signal_name,
                        parameters, user_data):
        """
        Called whenever a sleep (suspend/hibernate) is performed
        params[0] == True -> before sleep
        params[0] == False -> after sleep (waking up)
        """

        waking_up = not(parameters[0])  # True before sleep; False after wakeup

        if waking_up:
            self.app.emit("keyboard-added")

    def login_manager_name_appeared(self, connection, name, name_owner):
        """
        LoginManager bus name showed up -- connect sleep signal
        """
        self.subscriber_id = \
            connection.signal_subscribe(name_owner,
                                        "org.freedesktop.login1.Manager",
                                        "PrepareForSleep",
                                        "/org/freedesktop/login1",
                                        None,
                                        Gio.DBusCallFlags.NONE,
                                        self.on_sleep_signal,
                                        None)

    def login_manager_name_desappeared(self, connection, name, name_owner):
        """
        Login manager name disappeared -- not sure when this should happen if at all
        """
        connection.signal_unsubscribe(self.subscriber_id)
        self.sleep_subscriber_id = 0



