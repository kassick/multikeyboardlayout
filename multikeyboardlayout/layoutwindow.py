#!/usr/bin/env python
# -*- coding: utf-8 -*-

from gi.repository import GObject
from gi.repository import Gtk
from gi.repository import GnomeDesktop
from utils import *
from xinput import *
import sys

class LayoutLine(Gtk.ListBoxRow):
    def __init__(self, kbd_name, kbd_layout, kbd_variant, win):
        """

        :type win: LayoutWindow
        """

        Gtk.ListBoxRow.__init__(self)

        self.kbd_name = kbd_name
        self.kbd_layout = kbd_layout
        self.kbd_variant = kbd_variant
        self.win = win

        layout_name = kbd_layout
        if not(kbd_variant is None) and (len(kbd_variant) > 0):
            layout_name = layout_name + "+"+kbd_variant

        xkb_info = GnomeDesktop.XkbInfo()
        (ret, display_name, ignore1, ignore2, ignore3) = \
            xkb_info.get_layout_info(layout_name)
        if ret:
            self.display_name = display_name
        else:
            self.display_name = layout_name

        hbox = Gtk.HBox.new(homogeneous=False, spacing=10)

        line_text = "<b>%s</b> using layout <b>%s</b>" % \
            (self.kbd_name, self.display_name)
        kbd_label = Gtk.Label.new(line_text)
        kbd_label.set_use_markup(True)

        erase_btn = Gtk.Button.new_from_icon_name("gtk-delete",
                                                  Gtk.IconSize.BUTTON)

        erase_btn.connect("clicked", self.on_btn_erase_clicked)

        hbox.pack_start(kbd_label, True, False, 20)
        hbox.pack_start(erase_btn, False, False, 20)

        hbox.set_halign(Gtk.Align.END)

        self.add(hbox)

    def on_btn_erase_clicked(self, widget):
        try:
            self.win.app.conf_manager.remove_layout(self.kbd_name)
        except:
            Gtk.MessageDialog (self.win,
                               Gtk.DialogFlags.DESTROY_WITH_PARENT,
                               Gtk.MessageType.ERROR,
                               Gtk.ButtonsType.CLOSE,
                               "Could not write the configuration file").run()

        self.win.emit("reload_keyboards")

        return True


class NewLayoutLine(Gtk.ListBoxRow):
    def __init__(self, available, win):
        Gtk.ListBoxRow.__init__(self)
        self.win = win
        self.kbd_name_entry = None
        self.kbd_layout_entry = None

        def pack_hbox(w1, w2):
            w = Gtk.HBox(homogeneous=False, spacing=0)
            w.pack_start(w1, expand=False, fill=False, padding=0)
            w.pack_start(w2, expand=True, fill=True, padding=5)
            return w

        hbox = Gtk.HBox.new(homogeneous=False, spacing=10)

        kbd_name_entry = Gtk.ComboBoxText.new_with_entry()
        for (kbd_name, kbd_id) in available:
            kbd_name_entry.append(kbd_id, kbd_name)

        kbd_layout_label = Gtk.Label("Layout:")
        kbd_layout_text  = Gtk.ComboBoxText.new()
        kbd_layout_text.set_id_column(0)

        model = get_populated_layouts()
        # kbd_layout_combo = Gtk.ComboBox.new_with_model_and_entry(model)
        # renderer_text = Gtk.CellRendererText()
        # kbd_layout_combo.pack_start(renderer_text, False)
        # kbd_layout_combo.add_attribute(renderer_text, "text", 1)
        # kbd_layout_combo.set_id_column(0)
        # kbd_layout_combo.set_entry_text_column(1)
        #

        # A Gtk.Entry with a completion

        kbd_layout_completion = Gtk.EntryCompletion()
        kbd_layout_completion.set_model(model)
        kbd_layout_completion.set_popup_completion(True)

        renderer_text = Gtk.CellRendererText()
        kbd_layout_completion.pack_start(renderer_text, False)
        kbd_layout_completion.add_attribute(renderer_text, "text", 0)

        kbd_layout_completion.set_text_column(1)

        def filter_funct(compl, key, it, data):
            key_lower = key.lower()
            val0 = compl.get_model().get_value(it, 0).lower()
            val1 = compl.get_model().get_value(it, 1).lower()
            return (val0.find(key_lower) >= 0 ) or \
                   (val1.find(key_lower) >= 0 )

        kbd_layout_completion.set_match_func(filter_funct, None)
        #    lambda compl, key, it, data: \
        #        key in compl.get_model().get_value(it, 1),
        #        None)


        kbd_layout_entry = Gtk.Entry()
        kbd_layout_entry.set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, "search")
        kbd_layout_entry.set_completion(kbd_layout_completion)

        kbd_layout_entry.set_placeholder_text("Language or Layout Name")

        apply_btn = Gtk.Button.new_from_icon_name("dialog-apply",
                                                  Gtk.IconSize.BUTTON)
        apply_btn.connect("clicked", self.on_apply_btn_clicked)

        hbox.pack_start(kbd_name_entry,
                        expand=True, fill=True, padding=0)
        hbox.pack_start(pack_hbox(kbd_layout_label, kbd_layout_entry),
                        expand=True, fill=True, padding=0)
        # hbox.pack_start(pack_hbox(kbd_variant_label, kbd_variant_entry),
                        # expand=True, fill=True, padding=0)
        hbox.pack_start(apply_btn,
                        False, False, 20)

        self.add(hbox)
        self.kbd_layout_entry = kbd_layout_entry
        self.kbd_name_entry = kbd_name_entry


    def on_apply_btn_clicked(self, widget):
        # Searches the text on the liststore. If not found, assume the user
        # typed something of the kind us+intl and use it
        kbd_name = self.kbd_name_entry.get_active_text()
        layout_text = self.kbd_layout_entry.get_text()
        layout_variant_id = layout_text
        model = self.kbd_layout_entry.get_completion().get_model()

        if (len(kbd_name.strip()) == 0) or (len(layout_text.strip()) == 0):
            return True


        for (tmp_layout_id, layout_name) in model:
            if (layout_text == layout_name):
                layout_variant_id = tmp_layout_id


        try:
            (layout, variant) = layout_variant_id.split('+')
        except:
            layout = layout_variant_id
            variant = ""

        try:
            self.win.app.conf_manager.add_layout(kbd_name, layout, variant)
        except:
            Gtk.MessageDialog (self.win,
                               Gtk.DialogFlags.DESTROY_WITH_PARENT,
                               Gtk.MessageType.ERROR,
                               Gtk.ButtonsType.CLOSE,
                               "Could not write the configuration file").run()
        self.win.emit("reload_keyboards")
        map_keyboards_from_conf(self.win.app.conf_manager)

        return True






class LayoutWindow(Gtk.ApplicationWindow):
    __gsignals__ = {
        "reload_keyboards": (GObject.SIGNAL_RUN_FIRST, None,  () ),
    }
    def __init__(self, app):
        Gtk.ApplicationWindow.__init__(self, title="Multiple Keyboard Layouts", application=app)
        self.set_border_width(10)
        self.app = app

        header_bar1 = Gtk.HeaderBar()
        self.checkbox_session = Gtk.CheckButton.new_with_label("Start with session")
        header_bar1.add(self.checkbox_session)

        self.set_titlebar(header_bar1)
        header_bar1.set_title("Keyboard Layouts")
        header_bar1.set_show_close_button(True)

        scrolled_view = Gtk.ScrolledWindow.new(hadjustment=None, vadjustment=None)
        self.listbox = Gtk.ListBox.new()
        self.listbox.set_selection_mode(Gtk.SelectionMode.NONE)

        scrolled_view.add(self.listbox)

        self.add(scrolled_view)

        #self.setup_rows([("um", "dois", "tres"), ("lalala", "gb", "intl")], [ ("meu", "11"), ("outro", "24")])

        self.set_default_size(900,500)

        # Connect events
        self.connect("delete_event", self.on_delete)
        self.connect("show", self.on_show)

        self.connect("reload_keyboards", self.on_show)

        self.checkbox_session.connect("toggled", self.on_checkbox_session_toggled)


    def on_show(self, widget):
        self.checkbox_session.set_active(self.app.check_is_in_autostart())
        configured_keyboards = self.app.conf_manager.get_keyboards_layouts()
        available_keyboards = get_available_keyboards()

        self.setup_rows(configured_keyboards, available_keyboards)

    def on_checkbox_session_toggled(self, widget):
        session_enabled = self.app.check_is_in_autostart()
        if session_enabled and widget.get_active():
            # Paranoia check
            return True

        if widget.get_active():
            if not (self.app.enable_session_autostart()):
                # Un-checks the checkbox, avoid recursion -- or does gtk handle
                # this automatically ?
                self.checkbox_session.handler_block_by_func(self.on_checkbox_session_toggled)
                self.checkbox_session.set_active(False)
                self.checkbox_session.handler_unblock_by_func(self.on_checkbox_session_toggled)
        else:
            if not(self.app.disable_session_autostart()):
                self.checkbox_session.handler_block_by_func(self.on_checkbox_session_toggled)
                self.checkbox_session.set_active(True)
                self.checkbox_session.handler_unblock_by_func(self.on_checkbox_session_toggled)



    def setup_rows(self, layouts, available):
        for line in self.listbox.get_children():
            self.listbox.remove(line)

        for (kbd_name, kbd_layout, kbd_variant) in layouts:
            layout_line = LayoutLine(kbd_name, kbd_layout, kbd_variant, self)
            self.listbox.add(layout_line)

            separator = Gtk.ListBoxRow()
            separator.add(Gtk.HSeparator())

            self.listbox.add(separator)

        new_entry_line = NewLayoutLine(available, self)
        self.listbox.add(new_entry_line)

        self.listbox.show_all()

    def on_delete(self, widget, data):
        widget.hide()
        return True




__all__ = [ "LayoutWindow", "LayoutLine", "NewLayoutLine"]

