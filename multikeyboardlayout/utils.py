#!/usr/bin/env python
# -*- coding: utf-8 -*-

from gi.repository import Gtk
from gi.repository import GnomeDesktop
from xinput import *
import subprocess

def flatten(listOfLists):
    "Flatten one level of nesting"
    from itertools import chain
    return chain.from_iterable(listOfLists)

def get_populated_layouts():
    """
    Populates a comboentry with the available layouts

    """

    model = Gtk.ListStore(str,str)
    model.set_sort_column_id(1, Gtk.SortType.ASCENDING)


    xkb_info = GnomeDesktop.XkbInfo()
    infos = []
    for lid in xkb_info.get_all_layouts():
        (ret, display_name, short_name, xkb_layout, xkb_variant) = \
            xkb_info.get_layout_info(lid)

        if not(ret): continue

        infos.append( (lid,
                       display_name,
                       short_name,
                       xkb_layout,
                       xkb_variant) )

    for (lid, display_name, short_name, xkb_layout, xkb_variant) \
        in sorted(infos, lambda a,b: a[1] < b[1]):

        if not(ret): continue

        model.append([lid, display_name] )
                      #"%s (%s, aka %s)" %
                      #     (display_name, short_name, lid)  ])

    return model

def map_keyboards_from_conf(conf_manager):
    """
    Maps the currently connected keyboards to set up layouts

    :type conf_manager: kbd_conf_manager.KeyboardConfManager

    """
    configured = conf_manager.get_keyboards_layouts()
    available = get_available_keyboards()
    for (name, layout, variant) in configured:
        for (aname, aid) in available:
            if (aname == name):
                print "Will configure keyboard ",\
                       name,                     \
                       "id", aid,                \
                       "with layout", layout,    \
                       "variant", variant
                status = subprocess.call( ("setxkbmap -device %s %s %s" %
                                            (aid, layout, variant)).split(' '))

__all__ = [ "get_populated_layouts", "map_keyboards_from_conf" ]
