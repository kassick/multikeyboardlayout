#/usr/bin/env python
# -*- coding: utf-8 -*-


from gi.repository import GUdev
from gi.repository import GLib
import sys
import os
import re
import subprocess
import ConfigParser
from subprocess import Popen

CONF_FILE=os.path.expanduser("~/.config/keyboardlayouts.conf")

class KeyboardConfManager():
    """
    Class to manage the configurations of the mapped keyboards
    It provides methods to add and delete keyboard-to-layout mappings between
    the configuration file and  the configuration file
    """

    def __init__(self):

        self.conf = ConfigParser.ConfigParser()

        try:
            self.conf.read(CONF_FILE)
        except:
            print >>sys.stderr, "No conf file"

    def get_keyboards_layouts(self):
        """
        Returns a list of tuples (kbdname, layout, variant) known in the
        config file

        :rtype: [tuple(str, str, str)]
        """

        keyboards = []

        for kbdname in self.conf.sections():
            layout = self.conf.get(kbdname, "layout")
            try:
                variant = self.conf.get(kbdname, "variant")
                # @todo: See what's the expected behaviour and remove this test
                if variant is None:
                    print >> sys.stderr, "variant is none"
                    variant = ""
            except:
                variant = ""

            keyboards.append( (kbdname.strip(), layout.strip(), variant.strip()) )

        return keyboards

    def add_layout(self, kbdname, layout, variant):
        if (kbdname.strip() in self.conf.sections()):
            #print >> sys.stderr, \
            #    "Overriding an existing configuration -- should not happen!!!"
            self.conf.remove_section(kbdname)

        self.conf.add_section(kbdname)
        self.conf.set(kbdname, "layout", layout)
        if len(variant) > 0:
            self.conf.set(kbdname, "variant", variant)

        fp = open(CONF_FILE, 'w+')
        if not(fp):
            print >>sys.stderr, "Could not open config file for writing"
            raise Exception("Could not open config file for writing")

        self.conf.write(fp)

        fp.close()

    def remove_layout(self, kbdname):
        if not(kbdname in self.conf.sections()):
            print >>sys.stderr, "Asked to remove inexistent kbd ", kbdname
            return


        if not(self.conf.remove_section(kbdname)):
            print >>sys.stderr, "Not removed!", kbdname

        fp = open(CONF_FILE, 'w+')
        if not(fp):
            print >>sys.stderr, "Could not open config file for writing"
            raise Exception("Could not open config file for writing")

        self.conf.write(fp)

        fp.close()

__all__ = ["KeyboardConfManager"]
