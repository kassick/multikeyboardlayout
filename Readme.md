# Multi Keyboard Layout Configuration Utility

This utility provides a GUI to configure layouts for specific keyboards.
Most common use case: a notebook with a layout (say, Brazilian ABNT2) and a
usb keyboard with a layout US-International With Dead Keys.

It also works as a session daemon to be executed on login. It watches on
udev to check new HID devices, so you can set it up once, leave the program
running and use again only if you buy a new keyboard.

![Adding a new layout](https://bytebucket.org/kassick/multikeyboardlayout/raw/a5754e356963172eec8fcf98b765d3b02695c9af/add-new.png?token=cb325f6279b444c10d3af1ed1c4edbaeebe63e5f)

![Layout added](https://bytebucket.org/kassick/multikeyboardlayout/raw/9db75256c050783d48a27dec679667acd715d5e4/addded.png?token=9fa11c4e2013eae83c79d6b13a7738612aee80a6)

# How to use

*  Configure the layout for your notebok keyboard on your standard settings.
*  Connect the external keyboard
*  Run the utility
*  Select the external keyboard on the first entry
*  Type the language of the desired layout (or the layout code, if you know
   it)
*  Press Apply
*  Check "Start with Session" to make it start in daemon mode on login


# Requirements

*  xinput (/usr/bin/xinput)
*  x11-xkb-utils (/usr/bin/setxkbmap)
*  gir1.2-gudev-1.0 (GIR enabled udev library)
*  gir1.2-gtk-3.0 (GIR Gtk)
*  gir1.2-gnomedesktop-3.0 (GIR GnomeDesktop)
*  gir1.2-glib-2.0  (GIR GIO)
*  python-xdg
*  docopt
*  Python 2.7

# Notes

Keyboards are recognized by *name*. That may be a problem if you have two
keyboards that announce the same name for xinput -- both will end up with
the same layout. Further parsing xinput and correlating the input devices
with udev to allow matching by id, vendor, etc. is doable, but ended up in
the todo-list.

Configuration is stored in $HOME/.config/keyboardlayouts.conf .
It's a standard ini-like file. Each keyboard is a section. Options are
*layout*  and *variant*.
```
[My Keyboard Name]
layout=us
variant=intl
```

_xinput_ lists many things as keyboards (including my webcam). I haven't
filtered them (would require integration with udev as mentioned before).

   




