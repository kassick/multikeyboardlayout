#!/usr/bin/env python

from distutils.core import setup
#from DistUtilsExtra.command import *

setup(name='Multiple Keyboard Layout',
      version='1.0',
      description='Utility to map multiple layouts on a multi-keyboard environment',
      author='Rodrigo Virote Kassick',
      author_email='kassick@gmail.com',
      url='http://bitbucket.org/kassick/multikeyboardlayout',
      packages=['multikeyboardlayout'],
      scripts=['multikeyboardlayout_main'],
      data_files= [
                      ('share/applications',
                          ["MultipleKeyboardLayout.desktop"]),
                      ('share/multikeyboardlayout',
                          ["MultipleKeyboardLayout_session.desktop"]
                      )
                  ],
      # cmdclass = { 'build'       : build_extra.build_extra,
      #              "build_i18n"  : build_i18n.build_i18n,
      #              "build_help"  : build_help.build_help,
      #              "build_icons" : build_icons.build_icons,
      #             }
      )
